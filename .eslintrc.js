module.exports = {
  root: true,
  extends: '@react-native-community',
    parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  "lint": "eslint \"src/**/*.{ts,tsx}\"",
  "lint:fix": "eslint --fix \"src/**/*.{ts,tsx}\""
};
