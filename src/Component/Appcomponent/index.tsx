import React,{} from 'react';

import {View,
    SafeAreaView,
    Text,Pressable,TouchableOpacity,TextInput,Button,ActivityIndicator,ScrollView,FlatList,Image,ImageBackground} from 'react-native';



export const AppSafeAreaView=(props:any)=>{
    return(
        <SafeAreaView {...props}></SafeAreaView>  
    )
}


export const AppImageBackground=(props:any)=>{
    return(
        <ImageBackground {...props}></ImageBackground>  
    )
}

export const AppImage=(props:any)=>{
    return(
        <Image {...props}></Image>  
    )
}

export const AppFlatList=(props:any)=>{
    return(
        <FlatList {...props}></FlatList>  
    )
}

export const AppScrollView=(props:any)=>{
    return(
        <ScrollView {...props}></ScrollView>  
    )
}

export const AppActivityIndicator=(props:any)=>{
    return(
        <ActivityIndicator {...props}></ActivityIndicator>  
    )
}

export const AppButton=(props:any)=>{
    return(
        <Button {...props}></Button>  
    )
}

export  const AppView=(props:any)=>{
    return(
        <View {...props}></View>
    )
}

export const AppText=(props:any)=>{
    return(
        <Text {...props}></Text>
    )
}

export const AppTextInput=(props:any)=>{
    return(
        <TextInput {...props}></TextInput>
    )
}

export const AppTouchableOpacity=(props:any)=>{
    return(
        <TouchableOpacity {...props}></TouchableOpacity>
    )
}

export const AppPressable=(props:any)=>{
    return(
        <Pressable {...props}></Pressable>        
    )
}
