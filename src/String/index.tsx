// Screen name asking Location

export const str_your_location="Your Location";
export const con_Location='Confirm Your Location';
export const con_Locationonly='Confirm Location';
export const Your_Location="Your Location";
export const Edit_Btn="Edit";
export const select_location='Select Location';


//BottomSheet

export const str_Search_location='Search location';
export const location_Bottom="Your Location";
export const Change_Txt="Change";
export const current_Location= 'Use Current Location';
export const distance="8 km";
export const Place_name="Bandra Plot";
export const Address_Over="Jogeshwari East, India"


//AreaServiceable

export const Serviceable="Area Not Serviceable";
export const description="Scelerisque arcu, mattis eget proin nisl, malesuada urna mauris sit. Ultrices nunc, felis mattis gravida vel aliquam. Arcu, tellus mi cras adipiscing sed gravida orci turpis non. Eget elementum et euismod tortor fermentum. Sit posuere commodo orci quis vulputate. Sapien nisi, sit cursus arcu. Nisl curabitur a orci platea diam sociis nunc. Nec odio id in arcu, volutpat convallis in pulvinar sed.";
export const change_location="change Location"