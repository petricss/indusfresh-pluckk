import React from 'react';
import {Text, View, Dimensions, Image, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeRevamped from '../Screens/HomeFile/HomeRevamped';
// import Images from '../Theme/Image';
// import HomeRevamped from '../Screens/HomeFile/HomeRevamped';
// import HomeRevamped from '../Screens/HomeFile/HomeRevamped';

const Tab = createBottomTabNavigator();
const DeviceW = Dimensions.get('screen').width;
const RenderTabIcons = props => {
  const {icon, label, isFocused} = props;
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: DeviceW / 4,
        marginTop: 13,
      }}>
      <Image
        source={icon}
        style={{height: 24, width: 24, resizeMode: 'contain'}}
      />
      <Text
        style={[styles.labelText, {color: isFocused ? '#ef476f' : '#565656'}]}>
        {' '}
        {label}
      </Text>
    </View>
  );
};
const ActiveNavigator = createStackNavigator();
function stackNavigatorActive() {
  return (
    <ActiveNavigator.Navigator>
      <ActiveNavigator.Screen
        name="HomeRevamped"
        component={HomeRevamped}
        options={{headerShown: false}}
      />
    </ActiveNavigator.Navigator>
  );
}
const RankingNavigator = createStackNavigator();
function stackNavigatorHome() {
  return (
    <RankingNavigator.Navigator>
      <RankingNavigator.Screen
        name="HomeRevamped"
        component={HomeRevamped}
        options={{headerShown: false}}
      />
    </RankingNavigator.Navigator>
  );
}
const CollobrationsoutNavigator = createStackNavigator();
function stackNavigatorCollobrationsout() {
  return (
    <CollobrationsoutNavigator.Navigator>
      <CollobrationsoutNavigator.Screen
        name="HomeRevamped"
        component={HomeRevamped}
        options={{headerShown: false}}
      />
    </CollobrationsoutNavigator.Navigator>
  );
}

export default class ActiveTabs extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Tab.Navigator
          tabBarOptions={{
            style: {
              height: 50,
            },
          }}>
          <Tab.Screen
            name="HomeTab"
            component={stackNavigatorHome}
            options={{
              headerShown: false,
              tabBarLabel: '',
              tabBarIcon: ({focused}) => {
                return (
                  <RenderTabIcons
                    icon={focused ? require('../Assets/images/AskingUserLocation/distance.png') :require('../Assets/images/AskingUserLocation/distance.png')}
                    label={'HomeRevamped'}
                    isFocused={focused}
                  />
                );
              },
            }}
          />

          <Tab.Screen
            name="ProfileTab"
            component={stackNavigatorActive}
            options={{
              headerShown: false,
              tabBarLabel: '',
              tabBarIcon: ({focused}) => {
                return (
                  <RenderTabIcons
                    icon={focused ? require('../Assets/images/AskingUserLocation/distance.png') : require('../Assets/images/AskingUserLocation/distance.png')}
                    label={'HomeRevamped'}
                    isFocused={focused}
                  />
                );
              },
            }}
          />

          <Tab.Screen
            name="SettingTab"
            component={stackNavigatorCollobrationsout}
            options={{
              headerShown: false,
              tabBarLabel: '',
              tabBarIcon: ({focused}) => {
                return (
                  <RenderTabIcons
                    icon={focused ? require('../Assets/images/AskingUserLocation/distance.png') : require('../Assets/images/AskingUserLocation/distance.png')}
                    label={'HomeRevamped'}
                    isFocused={focused}
                  />
                );
              },
            }}
          />
        </Tab.Navigator>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  labelText: {
    fontSize: 10,
    // fontFamily:,
  },
});