import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScreenBottomTab, StackHome ,ScreenAskingUserLocation,ScreenAreaServiceable} from '../StackName';
const Stack = createNativeStackNavigator();
import AreaServiceable from '../Screens/Home/AreaServiceable';
import AskingUserLocation from '../Screens/Home/AskingUserLocation';

// Screen Import Here
import BottomTab from './BottomTab';

// https://itnext.io/change-react-native-screen-animation-direction-with-react-navigation-8cec0f66f22

const forFade = ({ current, layouts }) => ({
    cardStyle: {
        transform: [
            {
                translateX: current.progress.interpolate({ inputRange: [0, 1], outputRange: [layouts.screen.width, 0], }),
            },
        ],
    },
});
export const MainStack = () => {

    return (
        <Stack.Navigator
            mode={'modal'}
            name={StackHome}
            initialRouteName={ScreenAskingUserLocation}>
            <Stack.Screen
                options={{
                    cardStyleInterpolator: forFade,
                    headerBackTitle: '',
                    headerTitle: '',
                    headerShown: false
                }}
                name={ScreenAskingUserLocation} component={AskingUserLocation} ></Stack.Screen>

            <Stack.Screen
                options={{
                    cardStyleInterpolator: forFade,
                    headerBackTitle: '',
                    headerTitle: '',
                    headerShown: false
                }}
                name={ScreenAreaServiceable} component={AreaServiceable} ></Stack.Screen>
        </Stack.Navigator>
    )
}