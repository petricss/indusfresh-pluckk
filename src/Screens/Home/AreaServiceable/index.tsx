import React, { useState, useEffect } from 'react';
import { View, Platform } from 'react-native';
import style from './style';
import { AppView, AppText, AppImage, AppTextInput } from '../../../Component/Appcomponent';
import { Serviceable, description, change_location } from '../../../String';

const index = () => {
  return (
    <AppView style={style.MainView}>
      <AppImage style={style.centerImg} source={require('../../../Assets/images/AreaServiceable/service.png')}/>
      <AppText style={style.headTxt}>{Serviceable}</AppText>
      <AppText style={style.discrptionTxt}>{description}</AppText>
      <AppView style={style.btnView}>
        <AppText style={style.btnTxt}>{change_location}</AppText>
      </AppView>
    </AppView>
  )
}
export default index
