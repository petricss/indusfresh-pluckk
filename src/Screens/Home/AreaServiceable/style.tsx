import React, { } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { color_while } from '../../../Util/Colors';
import { Arvo_Regular_400, Montserrat_Regular_400, Arvo_bold_700 } from '../../../Util/Fonts';
import { deviceBasedDynamicDimension, normalize } from '../../../Util/Scalling';
const style = StyleSheet.create({
    MainView: {
        flex: 1,
        backgroundColor: color_while,
    },
    centerImg: {
        height: deviceBasedDynamicDimension(200, true, 1),
        width: 285,
        resizeMode: 'cover',
        alignSelf: 'center',
        marginTop: deviceBasedDynamicDimension(176, true, 1)
    },
    headTxt: {
        fontSize: normalize(24),
        lineHeight: 30,
        textAlign: 'center',
        marginTop: deviceBasedDynamicDimension(30, true, 1),
        color: '#000000',
        // fontWeight:'700',
        fontFamily: Arvo_bold_700,
        letterSpacing:1
    },
    discrptionTxt: {
        fontSize: normalize(14),
        lineHeight: 17,
        marginHorizontal: 17,
        textAlign: 'center',
        marginTop: deviceBasedDynamicDimension(15, true, 2),
        fontFamily: Montserrat_Regular_400
    },
    btnTxt:{
        fontSize:normalize(16),
        lineHeight:20,
        letterSpacing:1,
        color:'#233930',
        fontFamily: Arvo_bold_700
    },
    btnView:{
        marginHorizontal:50,
        backgroundColor:'#95E8BE',
        alignItems:'center',
        paddingVertical:13,
        borderRadius:15,
        marginTop:deviceBasedDynamicDimension(60, true, 1)
    }

});
export default style