import React from 'react';
import {StyleSheet, Platform,Dimensions} from 'react-native';
import {color_while, color_black} from '../../../Util/Colors';
import {
  Arvo_bold_700,
  Montserrat_Regular_400,
  Montserrat_SemiBold_600,
  Arvo_Regular_400,
  Montserrat_Medium_500,
} from '../../../Util/Fonts';
import {deviceBasedDynamicDimension, normalize} from '../../../Util/Scalling';
const style = StyleSheet.create({
  MainView: {
    flex: 1,
    backgroundColor: color_while,
    flexDirection: 'column',
    // backgroundColor:'red',
    alignItems: 'flex-start',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    // marginBottom: deviceBasedDynamicDimension(100, true, 1),
    // alignSelf:'flex-start',
    backgroundColor: 'red',
  },
  markerIcon:{height:deviceBasedDynamicDimension(201, true, 1), width:201, resizeMode:'contain',},
  BottomViews: {
    borderTopLeftRadius: deviceBasedDynamicDimension(15, true, 1),
    borderTopRightRadius: deviceBasedDynamicDimension(15, true, 1),
    backgroundColor: 'white',
    // width: '100%',
    // minHeight: deviceBasedDynamicDimension(Dimensions.get("window").height/1.8, true, 1),
    // height: deviceBasedDynamicDimension(Dimensions.get("window").height/1.8, true, 1),
    width: '100%',
    // flexDirection:'row',

    ...Platform.select({
      android: {
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
      },
      ios: {
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 1,
      },
    }),

    position: 'absolute',
    top: "73%",
    left: 0,
    bottom: 0,
    right: 0,
  },
  confirmTxt: {
    marginTop: deviceBasedDynamicDimension(24, true, 1),
    fontFamily: Arvo_bold_700,
    // fontWeight:'700',
    color: color_black,
    fontSize: normalize(15),
    lineHeight: 19,
  },
  bottomBorder: {borderBottomWidth: 1,backgroundColor:'#F2F2F2',borderBottomColor:'#F2F2F2' ,width: '100%'},
  locationTxt: {
    fontSize: normalize(12),
    marginTop: deviceBasedDynamicDimension(10, true, 2),
    lineHeight: 15,
    fontFamily: Montserrat_Regular_400,
    fontWeight: '500',
    color: '#000000',
  },
  locationIcon: {
    height: deviceBasedDynamicDimension(17, true, 1),
    width:deviceBasedDynamicDimension(14, true, 1),
    resizeMode:'stretch',
    alignSelf: 'center',
  },
  locationInput: {
    width:'75%',
    fontSize: normalize(14),
    lineHeight: 18,
    marginLeft: 10,
    color: '#000000',
    fontFamily: Arvo_bold_700,
  },
  editTxt: {
    color: '#1A90FC',
    alignContent:'flex-end',
    width:60,
    fontSize: normalize(12),
    lineHeight: 14,
    textAlign:'right',
    alignSelf: 'flex-end',
      //  backgroundColor:'red',
    fontFamily: Montserrat_Regular_400,
    fontWeight:'500'
  },
  buttonView: {
    paddingVertical: deviceBasedDynamicDimension(12, true, 1),
    marginTop: deviceBasedDynamicDimension(20, true, 1),
    borderRadius: deviceBasedDynamicDimension(15, true, 1),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#A8E6C1',
  },

  butoonTxt: {
    color: '#233930',
    fontSize: normalize(16),
    fontFamily: Arvo_bold_700,
    // fontWeight: '700',
  },

  inputMainView: {
    flexDirection: 'row',
    // marginTop: deviceBasedDynamicDimension(9, true, 1),
    alignItem: 'cenetr',
    justifyContents: 'center',
  },
  TopViewMapView: {
    backgroundColor: 'green',
    flex: 1,
    width: '100%',
    // minHeight: deviceBasedDynamicDimension(110, true, 1),
    
    height: 'auto',
    flexDirection: 'column',
  },
  sheetLocationTxt: {
    fontSize: normalize(15),
    color: '#000000',
    lineHeight: 19,
    marginTop: deviceBasedDynamicDimension(20, true, 1),
    fontFamily: Arvo_bold_700,
  },
  borSheetView: {
    borderWidth: 0,borderBottomWidth: 1,backgroundColor:'#F2F2F2'
    ,borderColor: '#F2F2F2',
    marginTop: deviceBasedDynamicDimension(9, true, 1),
    borderBottomColor:'#F2F2F2',
  },
  searchIcon: {
    height: deviceBasedDynamicDimension(30, true, 1),
    width: 30,
    resizeMode: 'contain',
    alignSelf: 'center',
    top: 4,
  },
  SearchView: {
    paddingVertical: 0,
    paddingHorizontal: 15,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: deviceBasedDynamicDimension(9, true, 1),
    borderRadius: deviceBasedDynamicDimension(15, true, 1),
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.4,
    shadowRadius: 15,
    elevation: 5,
    height: 60,
  },
  searchInput: {
    width: '85%',
    marginLeft: 10,
    height: 40,
    color:'#22170A',
    fontSize:normalize(14),
    fontWeight:'500',
    fontFamily:Montserrat_Regular_400,
    lineHeight:17
  },
  loIcon: {
    // height: deviceBasedDynamicDimension(20, true, 1),
    width: deviceBasedDynamicDimension(16, true, 1),
    height: deviceBasedDynamicDimension(16, true, 1),
    resizeMode: 'contain',
    alignSelf:'center',
    // backgroundColor:'red'
  },
  useLocationView: {
    marginTop: deviceBasedDynamicDimension(17, true, 1),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'flex-start',

    // backgroundColor:'red'
  },
  useText: {
    marginLeft: 9,
    color: '#1A90FC',
    fontSize: normalize(12),
    lineHeight: 16,
    height:16,
    paddingBottom:0,
    paddingTop:0,

    // top: 3,
    fontFamily: Montserrat_SemiBold_600,
    // fontWeight:'700'
    textAlignVertical:'center',
  },
  listMainView: {
    flexDirection: 'row',
    paddingVertical: deviceBasedDynamicDimension(4, true, 1),
    marginTop: deviceBasedDynamicDimension(10, true, 1),
    borderTopWidth: 1,
    // backgroundColor:'red',
    borderColor: '#F2F2F2',
  },
  Distance: {
    height:  deviceBasedDynamicDimension(19, true, 1),
    width:deviceBasedDynamicDimension(16, true, 1),
    resizeMode: 'contain',
  },
  distanceView: {
    paddingLeft:  deviceBasedDynamicDimension(2, true, 1),
    flex: 0.15,
    alignItems: 'flex-start',
    // backgroundColor:'green',
    marginTop: 10,
  },
  placeView: {
    flex: 0.85,
    marginTop: 10,
  },
  kmTxt: {
    fontSize: normalize(12),
    lineHeight: 14,
    marginTop: deviceBasedDynamicDimension(7, true, 1),
    color: '#000000',
    // fontWeight:'500',
    fontFamily: Montserrat_Medium_500
  },
  placeTxt: {
    fontSize: normalize(12),
    lineHeight: 14,
    fontFamily: Montserrat_Regular_400,
    color: '#000000',
    fontWeight:'700'
  },
  addressTxt: {
    fontSize: normalize(12),
    lineHeight: 14,
    marginTop: deviceBasedDynamicDimension(4, true, 1.5),
    fontWeight:'500',
    fontFamily:Montserrat_Regular_400,
    color:'#000000'
  },
  locationTxtBottom: {
    fontSize: normalize(12),
    marginTop: deviceBasedDynamicDimension(17, true, 1),
    lineHeight: 15,
    fontFamily: Montserrat_Regular_400,
    fontWeight: '500',
    color: '#000000',
  },
});
export default style;
