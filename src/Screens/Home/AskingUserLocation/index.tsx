import React, { useState, useEffect, useRef } from 'react';
import { View, Platform, PermissionsAndroid, Image } from 'react-native';
import style from './style';
import {
  str_your_location,
  con_Location,
  Your_Location,
  Edit_Btn,
  select_location,
  str_Search_location,
  location_Bottom,
  Change_Txt,
  current_Location,
  distance,
  Place_name,
  Address_Over,
  con_Locationonly,
} from '../../../String';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import {
  AppView,
  AppText,
  AppImage,
  AppTextInput,
  AppTouchableOpacity,
  AppFlatList,
  AppScrollView,
} from '../../../Component/Appcomponent';
import { Arvo_bold_700 } from '../../../Util/Fonts';
import { deviceBasedDynamicDimension, normalize } from '../../../Util/Scalling';
import RBSheet from 'react-native-raw-bottom-sheet';
import { ScreenAreaServiceable } from '../../../StackName';

const Data = [{ 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 }, { 1: 1 },];



const index = ({props,navigation}) => {
  const refRBSheet = useRef();
  const [location, setLocation] = useState('');
  const [Location] = useState(true);
  const [name, SetName] = useState('');
  useEffect(() => {
    requestLocationPermission();
  }, []);
  useEffect(() => { }, [name]);


  const ListItem = ( nav:any) => {
    return (
      <AppView style={style.listMainView}>
        <AppTouchableOpacity 
        onPress={()=>{
          refRBSheet?.current?.close()
          navigation.navigate(ScreenAreaServiceable)
        }}
        style={{flex:1,flexDirection:'row'}}> 
        <AppView style={style.distanceView}>
          < Image
            resizeMode='stretch'
            width={deviceBasedDynamicDimension(16, true, 1)}
            height={deviceBasedDynamicDimension(19, true, 1)}
            style={style.Distance}
            source={require('../../../Assets/images/AskingUserLocation/distance.png')}
          />
          <AppText style={style.kmTxt}>{distance}</AppText>
        </AppView>
        <AppView style={style.placeView}>
          <AppText style={style.placeTxt}>{Place_name}</AppText>
          <AppText style={style.addressTxt}>{Address_Over}</AppText>
        </AppView>
         </AppTouchableOpacity>  
      </AppView>
    );
  };

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' + 'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  return (
    <AppView style={style.MainView}>
      <AppView style={style.TopViewMapView}>
        <MapView
          style={style.map}
          showsUserLocation={false}
          showsMyLocationButton={false}
          showsScale={false}
          zoomControlEnabled={false}
          mapType={Platform.OS == 'android' ? 'standard' : 'standard'}
          provider={PROVIDER_GOOGLE}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker
            coordinate={{ latitude: 37.78825, longitude: -122.4324 }}
          >
            <AppImage
              style={style.markerIcon}
              source={require('../../../Assets/images/AskingUserLocation/marker.png')}
            />
          </Marker>
        </MapView>
        <AppView style={[style.BottomViews, { backgroundColor: 'white' }]}>
          {Location == false ? (
            <AppView style={{ marginHorizontal: 20 }}>
              <AppText style={style.confirmTxt}>{con_Location}</AppText>
              <AppView
                style={[style.bottomBorder, { marginTop: deviceBasedDynamicDimension(9, true, 1) }]}
              />
              <AppText style={[style.locationTxt]}>{Your_Location}</AppText>
              <AppView style={style.inputMainView}>
                {/* Assets/images/AskingUserLocation/Location.png */}
                <Image
                  style={style.locationIcon}
                  height={deviceBasedDynamicDimension(17, true, 0)}
                  width={deviceBasedDynamicDimension(14, true, 0)}
                  resizeMode={'stretch'}
                  resizeMethod={'scale'}
                  source={require('../../../Assets/images/AskingUserLocation/currentLocation.png')}
                />
                <AppTextInput
                  style={style.locationInput}
                  placeholder={'Dhaval Ganga Bandra West'}
                  onChangeText={txt => setLocation(txt)}
                  value={location}
                  editable={false}
                />
                <AppText style={[style.editTxt, { marginLeft: 13 }]}>{Edit_Btn}</AppText>
              </AppView>
              <AppView
                style={[style.bottomBorder, { marginTop: deviceBasedDynamicDimension(3, true, 1) }]}
              />
              <AppView style={style.buttonView}>
                <AppText style={style.butoonTxt}>{select_location}</AppText>
              </AppView>
            </AppView>
          ) : (
            <AppView style={{ marginHorizontal: 20 }}>
              <AppText style={style.confirmTxt}>Select {Your_Location}</AppText>
              <AppView
                style={[style.bottomBorder, { marginTop: deviceBasedDynamicDimension(8, true, 1) }]}
              />
              <AppText style={style.locationTxt}>{str_your_location}</AppText>
              <AppView style={style.inputMainView}>
                <AppImage
                  style={style.locationIcon}
                  height={deviceBasedDynamicDimension(17, true, 1)}
                  width={deviceBasedDynamicDimension(14, true, 1)}
                  source={require('../../../Assets/images/AskingUserLocation/currentLocation.png')}
                />
                <AppText
                  style={style.locationInput}

                  value={location}
                  editable={false}
                >
                  Dhaval Ganga Bandra ddd
                </AppText>
                <AppTouchableOpacity
                  style={{ alignSelf: 'center' }}
                  onPress={() => {
                    refRBSheet.current.open();
                  }}>
                  <AppText style={style.editTxt}>{Edit_Btn}</AppText>
                </AppTouchableOpacity>
              </AppView>
              <AppView
                style={[style.bottomBorder, { marginTop: deviceBasedDynamicDimension(3, true, 1) }]}
              />
              <AppTouchableOpacity
                onPress={() => {
                  requestLocationPermission();
                }}
                style={style.buttonView}>
                <AppText style={style.butoonTxt}>{con_Locationonly}</AppText>
              </AppTouchableOpacity>
            </AppView>
          )}
        </AppView>
      </AppView>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={700}
        customStyles={{
          container: {
            backgroundColor: '#FFFFFF',
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
          },
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: '#FFFFFF',
          },
        }}>
        <AppView style={{ marginHorizontal: 15, flex: 1 }}>
          <AppText style={style.sheetLocationTxt}>{str_Search_location}</AppText>
          <AppView style={style.borSheetView} />
          <AppText style={style.locationTxtBottom}>{location_Bottom}</AppText>
          <AppView style={style.inputMainView}>
            <AppImage
              style={style.locationIcon}
              source={require('../../../Assets/images/AskingUserLocation/currentLocation.png')}
            />
            <AppText
              style={style.locationInput}
              value={location}
              editable={false}
            >Dhaval Ganga Bandra West</AppText>
            <AppTouchableOpacity
              style={{ alignSelf: 'center' }}
              onPress={() => {
                // refRBSheet.current.open();
              }}>
              <AppText style={style.editTxt}>{Change_Txt}</AppText>
            </AppTouchableOpacity>
          </AppView>
          <AppView
            style={[style.bottomBorder, { marginTop: deviceBasedDynamicDimension(3, true, 1) }]}
          />
          <AppView style={style.SearchView}>
            <AppImage
              style={style.searchIcon}
              source={require('../../../Assets/images/AskingUserLocation/search.png')}
            />
            <AppTextInput style={style.searchInput} placeholder={'Bandr |'} />
          </AppView>
          <AppView style={style.useLocationView}>
            <AppImage
              height={16}
              width={16}
              style={style.loIcon}
              source={require('../../../Assets/images/AskingUserLocation/location.png')}
            />
            <AppText style={style.useText}>{current_Location}</AppText>
          </AppView>
          <AppView
            style={{ marginTop: deviceBasedDynamicDimension(5, true, 1) }}>
            <AppScrollView nestedScrollEnabled={true} style={{}}>
              <AppFlatList
                data={Data}
                renderItem={()=>{
                  return <ListItem nav={()=>{
                    props.navigation.navigate(ScreenAreaServiceable)
                  }} />
                }} />
            </AppScrollView>
          </AppView>
        </AppView>
      </RBSheet>
    </AppView>
  );
};
export default index;
