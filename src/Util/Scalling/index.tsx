
import React from 'react';
import { Platform, Dimensions, PixelRatio, } from 'react-native';
const { width: SCREEN_WIDTH } = Dimensions.get('window');
const scale = SCREEN_WIDTH / 375;
// import {
//     SafeAreaView, Dimensions, PixelRatio, View, Text, FlatList, SectionList,
//     StyleSheet, Button, TouchableOpacity, CheckBox, Switch, Platform, Image, TextInput, Picker, ActivityIndicator, Alert, ImageBackground
// } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

let screenWidth = Dimensions.get('window').width;
let screenHeight = Dimensions.get('window').height;


//Artboard Dimension 
let artBoardHeightOrg = 375;
let artBoardWidthOrg = 667;
//Re calculated Artboard Dimension 
let artBoardWidth = isSameRatio() ? artBoardWidthOrg : screenWidth;
let artBoardHeight = isSameRatio() ? artBoardHeightOrg : screenHeight;

function isSameRatio(): boolean {
    return artBoardWidthOrg / artBoardHeightOrg < 1 && screenWidth / screenHeight < 1
}

//Top or Bottom nav spaces or any extra space occupied by os e.g Status bar, Notch 
let extraSpace = 0;

export function deviceBasedDynamicDimension(originalDimen: number, compareWithWidth: boolean, resizeFactor: number): number | undefined {
    if (originalDimen != null) {
        if (resizeFactor != null) {
            originalDimen *= resizeFactor;
        }
        const compareArtBoardDimenValue = compareWithWidth ? artBoardWidth : artBoardHeight;
        const artBoardScreenDimenRatio = (originalDimen * 100) / compareArtBoardDimenValue;
        let compareCurrentScreenDimenValue = compareWithWidth ? screenWidth : screenHeight - extraSpace;
        if (Platform.OS === 'web') {
            return (responsiveWidth(originalDimen / compareCurrentScreenDimenValue) * 100);
        }
        return PixelRatio.roundToNearestPixel((artBoardScreenDimenRatio * compareCurrentScreenDimenValue) / 100,);
    }
    return null;
}

export function normalize(size) {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
          return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    } else {
          return Math.round(PixelRatio.roundToNearestPixel(newSize));
    }
}

