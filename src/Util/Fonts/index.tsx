
export const Arvo_Regular_400="Arvo-Regular-400";
export const Arvo_bold_700="Arvo-Bold-700";
export const Arvo_bolditelic_700="Arvo-BoldItalic-700";
export const Arvo_Itelic_400="Arvo-Italic-400";


export const Monserrat_thikItlic_100="Montserrat-ThinItalic-100";
export const Monserrat_black_900="Montserrat-Black-900";
export const Monserrat_blackItelinc_900="Montserrat-BlackItalic-900";
export const Monserrat_Bold_700="Montserrat-Bold-700";
export const Monserrat_Bold_Itelic_700="Montserrat-BoldItalic-700";
export const Monserrat_ExtraBold_800="Montserrat-ExtraBold-800";

export const Monserrat_ExtraBoldItelic_800="Montserrat-ExtraBoldItalic-800";
export const Monserrat_ExtraLight_200="Montserrat-ExtraLight-200";
export const Monserrat_ExtraLightItelic_200="Montserrat-ExtraLightItalic-200";

export const Monserrat_Light_300="Montserrat-Light-300";
export const Monserrat_LightItalic_300="Montserrat-LightItalic-300";
export const Montserrat_Medium_500="Montserrat-Medium-500";

export const Montserrat_MediumItalic_500="Montserrat-MediumItalic-500";
export const Montserrat_Regular_400="Montserrat-Regular-400";
export const Montserrat_SemiBold_600="Montserrat-SemiBold-600";

export const Montserrat_SemiBoldItalic_600="Montserrat-SemiBoldItalic-600";
export const Montserrat_Thin_100="Montserrat-Thin-100";
// export const Montserrat_SemiBold_600="Montserrat-SemiBold-600.ttf";

/*
export const Arvo_Regular_400="Arvo-Regular-400.ttf";
export const Arvo_bold_700="Arvo-Bold-700.ttf";
export const Arvo_bolditelic_700="Arvo-BoldItalic-700.ttf";
export const Arvo_Itelic_400="Arvo-Italic-400.ttf";


export const Monserrat_thikItlic_100="Montserrat-ThinItalic-100.ttf";
export const Monserrat_black_900="Montserrat-Black-900.ttf";
export const Monserrat_blackItelinc_900="Montserrat-BlackItalic-900.ttf";
export const Monserrat_Bold_700="Montserrat-Bold-700.ttf";
export const Monserrat_Bold_Itelic_700="Montserrat-BoldItalic-700.ttf";
export const Monserrat_ExtraBold_800="Montserrat-ExtraBold-800.ttf";

export const Monserrat_ExtraBoldItelic_800="Montserrat-ExtraBoldItalic-800.ttf";
export const Monserrat_ExtraLight_200="Montserrat-ExtraLight-200.ttf";
export const Monserrat_ExtraLightItelic_200="Montserrat-ExtraLightItalic-200.ttf";

export const Monserrat_Light_300="Montserrat-Light-300.ttf";
export const Monserrat_LightItalic_300="Montserrat-LightItalic-300.ttf";
export const Montserrat_Medium_500="Montserrat-Medium-500.ttf";

export const Montserrat_MediumItalic_500="Montserrat-MediumItalic-500.ttf";
export const Montserrat_Regular_400="Montserrat-Regular-400.ttf";
export const Montserrat_SemiBold_600="Montserrat-SemiBold-600.ttf";

export const Montserrat_SemiBoldItalic_600="Montserrat-SemiBoldItalic-600.ttf";
export const Montserrat_Thin_100="Montserrat-Thin-100.ttf";
// export const Montserrat_SemiBold_600="Montserrat-SemiBold-600.ttf";*/ 