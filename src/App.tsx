/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useLayoutEffect } from 'react';

import {
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import SplashScreen from 'react-native-splash-screen';
import { AppSafeAreaView } from './Component/Appcomponent';
import { NavigationContainer } from '@react-navigation/native';

//  Import the Stack Here
import { MainStack } from './Router'
const App = ({props,navigation}) => {

  //  Declare The Location Variable Here
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };


  useLayoutEffect(()=>{
      navigation?.setOptions({
        headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
        headerBackTitleStyle: {
            opacity: 0,
        },
        headerTintColor: '#000000'
      })
  },[])

  useEffect(() => {

    // Hide The Splash SCreen Here
    setTimeout(() => {
      SplashScreen.hide();
    }, 1000)
  }, [])


  //  Start render Method Here
  return (
    <AppSafeAreaView style={styles.safeAreaStyle}>
      {
        //  Here is Check the OS is android or not
        Platform.OS == "android" &&
        <StatusBar 
        barStyle={ 'dark-content'} 
        // barStyle={isDarkMode ? 'light-content' : 'dark-content'} 
        translucent={true}
        // barStyle={'default'}

        // backgroundColor= "#105846"
        //  backgroundColor="transparent"
        />
      }

      <NavigationContainer>
        <MainStack></MainStack>
      </NavigationContainer>

    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
